#' Dead simple "message" logging.
#'
#' @name message
#'
#' @description
#' These functions aim to provide a base logging interface for users using a
#' familiar "API". They are not intended to be used for any serious logging
#' activities that require advanced features, such as asynchronous or fail-safe
#' logging. Speed was also not a consideration in the design of these functions.
#'
#' @details
#' There are five (5) log levels, with corresponding function names, which in
#' order of increasing severity are: [debug()], [message()], [warning()],
#' [error()], and [fatal()]. Error and fatal error messages and warnings will
#' always be written to the appropriate connection (see below). Messages and
#' debug messages will only be written to this connection if value of their
#' `verbosity` is high enough.
#'
#' By default, all messaging functions will inherit their message connection
#' and the verbosity level from the calling environment. The message connection
#' will inherit from the variable `log.file`, while the verbosity inherits from
#' `log.level`. Whenever either of these variables are not defined (or otherwise
#' uninheritable), the values [stderr()] and `0` are substituted, respectively.
#' These values may be changed on a per-call basis, via the `file` and
#' `verbosity` arguments.
#'
#' Some of these functions mask built-in functions in the base R package. Both
#' [warning()] and[message()] *should* be compatible with these built-ins. The
#' [debug()] function is not similarly backwards compatible, although this
#' function should probably(?) not be used in any released code; developers can
#' still use [base::debug()] for this purpose.
#'
#' The built-in functions for silencing messages ([base::suppressWarnings()] and
#' [base::suppressMessages()]) should likewise work as before with this package.
#'
#' @seealso [base::message()] [base::warning()] [simpleMessage()]
NULL

# The default message connection
DEFAULT_MESSAGE_CONNECTION <- stderr()

# The default verbosity level
DEFAULT_VERBOSITY <- 0

#' @rdname message
#' @export message
#' @usage
#'   message(...,file = ,verbosity = ,call. = NULL,domain = NULL,appendLF = NULL)
#' @param file
#'   The file (connection) to which the log message is written.
#' @param verbosity
#'   The verbosity level to apply this message.
#' @param call.
#'   The name of the calling function; `NULL` extracts the name from the call
#'   stack (via [tracr::caller()]).
#' @param domain
#'   Translate log message via NLS (see: [base::gettext()]).
#' @param appendLF
#'   IGNORED. This option exists for backwards compatibility with built-in
#'   functions with the same names. See the [details] section for more
#'   information about line breaks in messages.
message <- function (...,
  file = dynGet("log.file",inherits = TRUE,ifnotfound = loggr:::DEFAULT_MESSAGE_CONNECTION),
  verbosity = dynGet("log.level",inherits = TRUE,ifnotfound = loggr:::DEFAULT_VERBOSITY),
  call. = NULL,domain = NULL,appendLF = NA
  ) {
  # translate objects into a message (condition) via simpleMessage
  arguments <- list(...)
  conditions <- lapply(
    X = arguments,
    FUN = simpleMessage,
    call = if (is.null(call.)) tracr::caller(-4) else if (isFALSE(call.)) "" else call.
    )
  # specify the default handler
  withRestarts(
    expr = lapply(
      X = conditions,
      FUN = function (condition) {
        if (verbosity >= 1) {
          signalCondition(condition)
          print.message(condition)
        }
      }
      ),
    muffleMessage = function () NULL
    )
  return(invisible(sapply(conditions,`[[`,"message")))
}

#' @rdname message
#' @export warning
#' @export warningMessage
#' @usage
#'   warning(...,file = ,verbosity = ,call. = NULL,immediate. = FALSE,
#'     noBreaks. = TRUE,domain = NULL,appendLF = NULL)
#' @param immediate.
#'   IGNORED. This named argument exists only for "compatibility" with existing
#'   functions. Warnings are always logged immediately.
#' @param noBreaks.
#'   IGNORED. This option exists for backwards compatibility with built-in
#'   functions with the same names. See the [details] section for more
#'   information about line breaks in messages.
warning <- warningMessage <- function (...,
  file = dynGet("log.file",inherits = TRUE,ifnotfound = loggr:::DEFAULT_MESSAGE_CONNECTION),
  verbosity = dynGet("log.level",inherits = TRUE,ifnotfound = loggr:::DEFAULT_VERBOSITY),
  call. = NULL,immediate. = FALSE,noBreaks. = FALSE,
  domain = NULL,appendLF = NA
  ) {
  # translate objects into a warning (condition) via simpleWarning
  arguments <- list(...)
  conditions <- lapply(
    X = arguments,
    FUN = simpleWarning,
    call = if (is.null(call.)) tracr::caller(-4) else if (isFALSE(call.)) "" else call.
    )
  # specify the default handler
  withRestarts(
    lapply(
      X = conditions,
      FUN = function (condition) {
        signalCondition(condition)
        print.message(condition)
      }
      ),
    muffleWarning = function () NULL,
    muffleMessage = function () NULL
    )
  return(invisible(sapply(conditions,`[[`,"message")))
}

#' @rdname message
#' @export error
#' @export errorMessage
#' @usage
#'   error(...,file = ,verbosity = ,call. = NULL,immediate. = FALSE,
#'     noBreaks. = TRUE,domain = NULL,appendLF = NULL)
error <- errorMessage <- function (...,
  file = dynGet("log.file",inherits = TRUE,ifnotfound = loggr:::DEFAULT_MESSAGE_CONNECTION),
  verbosity = dynGet("log.level",inherits = TRUE,ifnotfound = loggr:::DEFAULT_VERBOSITY),
  call. = NULL,immediate. = FALSE,noBreaks. = FALSE,
  domain = NULL,appendLF = NA
  ) {
  # translate objects into an error (condition) via simpleError
  arguments <- list(...)
  conditions <- lapply(
    X = arguments,
    FUN = simpleError,
    call = if (is.null(call.)) tracr::caller(-4) else if (isFALSE(call.)) "" else call.
    )
  # specify the default handler
  withRestarts(
    lapply(
      X = conditions,
      FUN = function (condition) {
        signalCondition(condition)
        print.message(condition)
      }
      ),
    muffleError = function () NULL,
    muffleWarning = function () NULL,
    muffleMessage = function () NULL
    )
  bak <- options(show.error.messages = FALSE)
  on.exit(options(bak))
  stop(invisible(sapply(conditions,`[[`,"message")))
}

#' @rdname message
#' @export fatal
#' @export fatalMessage
#' @usage
#'   fatal(...,status = 255,file = ,verbosity = ,call. = NULL,domain = NULL,
#'     appendLF = NULL)
#' @param status
#'   The exit status. See [base::quit()] for more details about appropriate
#'   values for this argument.
fatal <- fatalMessage <- fatalErrorMessage <- function (...,status = 255,
  file = dynGet("log.file",inherits = TRUE,ifnotfound = loggr:::DEFAULT_MESSAGE_CONNECTION),
  verbosity = dynGet("log.level",inherits = TRUE,ifnotfound = loggr:::DEFAULT_VERBOSITY),
  call. = NULL,domain = NULL,appendLF = NA
  ) {
  # translate objects into an error (condition) via simpleError
  arguments <- list(...)
  conditions <- lapply(
    X = arguments,
    FUN = simpleError,
    call = if (is.null(call.)) tracr::caller(-4) else if (isFALSE(call.)) "" else call.
    )
  # specify the default handler
  withRestarts(
    lapply(
      X = conditions,
      FUN = function (condition) {
        signalCondition(condition)
        print.message(condition)
        # don't `quit` here, let all messages `print` first
      }
      ),
    muffleError = function () NULL,
    muffleWarning = function () NULL,
    muffleMessage = function () NULL
    )
  quit(save = "no",status = status)
}

#' @rdname message
#' @export debug
#' @export debugMessage
#' @usage
#'   debug(...,file = ,verbosity = ,domain = NULL,appendLF = NULL)
debug <- debugMessage <- function (...,
  file = dynGet("log.file",inherits = TRUE,loggr:::DEFAULT_MESSAGE_CONNECTION),
  verbosity = dynGet("log.level",inherits = TRUE,ifnotfound = loggr:::DEFAULT_VERBOSITY),
  call. = NULL,domain = NULL,appendLF = NA
  ) {
  # translate objects into a message (condition) via simpleDebug
  arguments <- list(...)
  conditions <- lapply(
    X = arguments,
    FUN = simpleDebug,
    call = if (is.null(call.)) tracr::caller(-4) else if (isFALSE(call.)) "" else call.
    )
  # specify the default handler
  withRestarts(
    lapply(
      X = conditions,
      FUN = function (condition) {
        if (verbosity >= 2) {
          signalCondition(condition)
          print.message(condition)
        }
      }
      ),
    muffleMessage = function () NULL
    )
  return(invisible(sapply(conditions,`[[`,"message")))
}
