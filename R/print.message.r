#' @inherit base::print
#' @export
#' @param file
#'   The file (connection) to which the log message is written.
print.message <- function (x,...,
  file = dynGet("log.file",inherits = TRUE,ifnotfound = loggr:::DEFAULT_MESSAGE_CONNECTION)
  ) {
  cat(x$prefix,x$message,"\n",file = file,append = TRUE,sep = "")
  return(invisible(x))
}
