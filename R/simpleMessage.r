#' Log message objects.
#'
#' @name simpleMessage
#'
#' @description
#' The following functions serve as log message generators that can be passed
#' around to  any of the log messaging functions provided by this package (e.g.
#' [message()] or [print.message()]). They are also [base::conditions()].
#'
#' @details
#' Log messages are S3 objects that piggy-back on base conditions. They contain
#' information about the message generated -- such as the date and time of
#' creation and the message log level -- in addition to the message itself.
#'
#' For now, all messages inherit from the abstract base classes "message" and
#' "condition" (in order of precedence). As with their base package equivalents,
#' warnings and errors (including fatal errors) inherit from the abstract base
#' classes "warning" and "error" respectively.
#'
#' @seealso [loggr::message()] [base::conditions()]
NULL

#' @rdname simpleMessage
#' @export simpleMessage
#' @usage
#'   simpleMessage(message,call = NULL,domain = NULL)
#' @param message
#'   An R object, coercible to a string (via [base::as.character()]).
#' @param call
#'   The calling expression. This option exists for backwards compatibility with
#'   built-in functions with the same names and should not be used under most
#'   circumstances. If `NULL`, the name of the calling function is deduced from
#'   the stack frame.
#' @param domain
#'   Translate log message via NLS (see: [base::gettext()]).
simpleMessage <- function (message,call = NULL,domain = NULL) {
  name <- "simpleMessage" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7),
    call = call,domain = domain
    )
}

#' @rdname simpleMessage
#' @export simpleWarning
#' @usage
#'   simpleWarning(message,call = NULL,domain = NULL)
simpleWarning <- function (message,call = NULL,domain = NULL) {
  name <- "simpleWarning" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7),
    call = call,
    domain = domain
    )
}

#' @rdname simpleMessage
#' @export simpleWarningMessage
#' @usage
#'   simpleWarningMessage(message,call = NULL,domain = NULL)
simpleWarningMessage <- function (message,call = sys.call(-1),domain = NULL) {
  name <- "simpleWarningMessage" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7,nchar(name) - 7),
    call = call,
    domain = domain
    )
}

#' @rdname simpleMessage
#' @export simpleError
#' @usage
#'   simpleError(message,call = NULL,domain = NULL)
simpleError <- function (message,call = NULL,domain = NULL) {
  name <- "simpleError" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7),
    call = call,
    domain = domain
    )
}

#' @rdname simpleMessage
#' @export simpleErrorMessage
#' @usage
#'   simpleErrorMessage(message,call = NULL,domain = NULL)
simpleErrorMessage <- function (message,call = NULL,domain = NULL) {
  name <- "simpleErrorMessage" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7,nchar(name) - 7),
    call = call,
    domain = domain
    )
}

#' @rdname simpleMessage
#' @export simpleDebug
#' @usage
#'   simpleDebug(message,call = NULL,domain = NULL)
simpleDebug <- function (message,call = NULL,domain = NULL) {
  name <- "simpleDebug" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7),
    call = call,
    domain = domain
    )
}

#' @rdname simpleMessage
#' @export simpleDebugMessage
#' @usage
#'   simpleDebugMessage(message,call = NULL,domain = NULL)
simpleDebugMessage <- function (message,call = NULL,domain = NULL) {
  name <- "simpleDebugMessage" # tracr::caller()
  .makeMessage(
    message = message,
    level = substring(name,7,nchar(name) - 7),
    call = call,
    domain = domain
    )
}

#" @usage
#"   .makeMessage(message,level,call,domain = NULL)
.makeMessage <- function (message,level,call,domain = NULL) {
  class <- tolower(level)
  if (inherits(message,class)) {
    # already a message of the proper type
    return(message)
  }
  else if (inherits(message,"message")) {
    message$level <- NULL # remove old level
    list2env(message,envir = environment())
  }
  # get the caller, unless provided
  caller <- if (is.null(call)) tracr::caller(-3) else call
  caller <- if(is.call(caller)) as.character(caller[[1]]) else caller
  # build the class "hierarchy"
  class <- class[class != "message"]
  class <- unique(c(class,"message","condition"))
  # build and return the message object
  structure(
    list(
      prefix = .makeMessagePrefix(level = toupper(level),caller = caller),
      message = base::.makeMessage(message,domain = domain,appendLF = FALSE),
      level = level,
      call = call
      ),
    class = class
    )
}

#" @usage
#"   .makeMessagePrefix(...,level = "MESSAGE",caller = tracr::caller(-2),
#"     domain = NULL,appendLF = NULL)
#" @param caller
#"   The name of the calling function.
#" @param appendLF
#"   IGNORED. This option exists for backwards compatibility with built-in
#"   functions with the same names. See the [details] section for more
#"   information about line breaks in messages.
.makeMessagePrefix <- function (...,
  level = "MESSAGE",
  caller = tracr::caller(-2),
  domain = NULL,
  appendLF = FALSE
  ) {
  # get a timestamp for this message
  datetime <- strftime(Sys.time(),"%F%t%T")
  # find the calling function name
  caller <- if (is.null(caller)) tracr::caller(-2) else caller
  caller <- if (is.call(caller)) as.character(caller[[1]]) else caller
  # find the calling file name
  script <- tracr::script()
  script <- if (is.na(script)) "R" else script
  return(sprintf("%s\t%s::%s\t[%s]\t",datetime,script,caller,level))
  # TODO find the calling line?
  line <- NaN
  return(sprintf("%s\t%s::%s::%i\t[%s]\t",datetime,script,caller,line,level))
}
