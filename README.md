<!-- DO NOT EDIT! README.md is auto-generated from README.rmd via: `rmarkdown::render("README.rmd")`. -->

# loggr

Dead simple logging facilities for R.

## Overview

loggr is an extremely simple package designed to make writing log
messages as straight-forward as possible. In fact, if all you care about
is logging warnings and errors, you don’t even have to do anything else
but install and import the package into your R scripts. Adding debug
messages is also very easy. Just call one of R’s built in “condition”
functions (e.g. `message` or `simpleMessage`) and loggr will take take
of everything for you.

All logging messages have been “standardized” into a common printing
format, so you won’t need to worry about which kinds of messages contain
what prefixes, where newlines are appended, or anything else. Messages
are auto-tagged with supporting information, including a timestamp, the
calling function and calling script, and the log-level. That way,
they’re easily searchable.

There are a couple of “configuration” options available to users,
namely:

  - The log file (where messages are printed by default)
  - The current verbosity level (which messages are printed/silenced)

Everything else was shed for the sake of simplicity of use. In
particular, this package does not make any attempt to implement more
complex logging facilities, such as asynchronous or fail-safe logging.
However, this package should be relatively easy to import into other
packages that offer these features.

## Installation

Currently, loggr is only available via GitLab @
[cljacobs/loggr](https://gitlab.com/cljacobs/loggr).

``` r
# Installing with devtools:
library(devtools)
devtools::install_gitlab("cljacobs/loggr")
```

## Usage

Using loggr is as simple as loading the package. By default, only
warnings, errors, and fatal errors are logged.

``` r
message("This message is silently ignored.")
debug("Debug messages are also silent by default.")
warning("This warning is *not* ignored.")
#> 2018-10-26   16:40:59    R::script   [WARNING]   This warning is *not* ignored.
error("Errors are always logged.")
#> 2018-10-26   16:40:59    R::script   [ERROR] Errors are always logged.
fatal("Fatal errors are always logged (and stop execution).")
#> 2018-10-26   16:40:59    R::script   [FATAL] Fatal errors are always logged (and stop execution).
```

You can turn off the silencing of messages on a call-by-call basis, or
by increasing the verbositry level:

``` r
message("This message will *not* be ignored anymore." , verbosity = 1)
#> 2018-10-26   16:40:59    R::script   [WARNING]   This message will *not* be ignored anymore.
message("This message *will* be ignored again.")
local({ log.level <- 1; message("This message will *not* be ignored, due to inheritance of the log.level variable") })
#> 2018-10-26   16:40:59    R::script   [WARNING]   This message will *not* be ignored, due to inheritance of the verbosity variable.
```

You can also redirect the output into any open connection:

``` r
warning("This warning prints to `stderr()`.")
#> 2018-10-26   16:40:59    R::script   [WARNING]   This warning prints to `stderr()`.
warning("This warning prints to 'log.txt'" , file = "log.txt")
local({ log.file <- "log.txt"; warning("This warning *also* prints to 'log.txt', due to inheritance of the log.file variable.") })
```

<!-- This file's format is shamelessly copied from the [tidyverse](https://www.tidyverse.org/) collection of packages. -->

<!-- To generate this Markdown file, run: `rmarkdown::render("README.rmd")`. Knitr doesn't currently do the "right" thing. -->
